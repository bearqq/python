#coding: utf-8
import TShelper
import logging

log = logging.getLogger('HOTKEY.main')

def main():
	log.critical(u'\n按下开始： Alt+Z\n按下结束： Alt+F12!\n当前版本：%s' % TShelper.ver)
	
	def trig():
		log.critical('Triggered')
	
	THE_EXIT=TShelper.make_flag()

	trun=TShelper.HK_run(THE_EXIT)#HK_run(THE_EXIT,90,win32con.MOD_ALT)
	trun.key=90#z
	trun.flag=TShelper.win32con.MOD_ALT
	trun.triggered=trig
	trun.start()
	
	TShelper.HK_exit_daemon(THE_EXIT)
	trun.join()

if __name__ == '__main__':
	main()