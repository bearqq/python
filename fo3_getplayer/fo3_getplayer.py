#coding: utf-8
"""
本脚本使用python27，支持windows。使用需安装python27以及插件pywin32，地址如下：
http://sourceforge.net/projects/pywin32/files/pywin32/
使用前还需下载天使插件TSPlug.dll，并注册Regsvr32 TSPlug.dll。
安装后直接双击脚本即可。按提示使用。
"""

#defines
ver=u'Fifa 自动购买+1球员 base r1'	#版本
singlethreat=True	#设置为False可以多次多地触发，按下停止后全部停止，但主程序需要再次按下触发按键才能退出。设置为True表示只能一个窗口触发主程序，可即时退出。
tdelay=5	#延迟时间，单位秒
offx=0	#x轴修正值，修改后按键位置会整体偏移
offy=0	#y轴修正值，修改后按键位置会整体偏移
indebug=True
usefav=True
playersinfav=27	#注意修改！

#imports
import ctypes, win32con, ctypes.wintypes, win32gui,sys
import win32com.client
import time
from ctypes import *
import threading
reload(sys)
sys.setdefaultencoding('utf-8')

#inits
EXIT=False
loopfav=[0]

try:
	#dm=win32com.client.Dispatch('dm.dmsoft')
	ts=win32com.client.Dispatch('ts.tssoft')
except:
	print u"请先注册插件 Regsvr32 TSPlug.dll"
	sys.exit()

ts.SetPath("fo3_getplayer_db")


def prtitle(str):print str#os.system("title %s" % str)
prtitle(ver)

"""
def TS_run():
	print "happy!"
	time.sleep(3)
"""

def TS_run(): #main function
	loop=0
	got=[0]
	wide=c_int(0)
	high=c_int(0)
	intX=c_int(0)
	intY=c_int(0)
	
	if usefav:
		global tdelay
		tdelay=1
	
	def gotogather():
		ts_ret=ts.FindPic(0, 0, 1280, 720, "zhenr.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))
		if ts_ret[1]>=0 and ts_ret[2]>=0:
			print u'\t自动进入阵容页面'
			ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#去阵容页面
			#time.sleep(1)
			ts.LeftClick()
		else:
			ts.MoveTo(752, 694)	#去阵容页面
			#time.sleep(1)
			ts.LeftClick()
		time.sleep(1)
		
		gomanage0=1
		while gomanage0:
			ts_ret=ts.FindPic(0, 0, 1280, 340, "gomanage1.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gomanage0=0
			else:
				prtitle(u'\t等待进入阵容管理页面')
				time.sleep(1)
		ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#选择阵容管理
		ts.LeftClick()
		
		gomanage1=1
		while gomanage1:
			ts_ret=ts.FindPic(0, 0, 1280, 340, "gomanage2.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gomanage1=0
			else:
				prtitle(u'\t等待进入强化页面')
				time.sleep(1)
		ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#选择强化
		ts.LeftClick()
	
	def gatherplus(bmpstr,gatherresult):
		gogather0=1
		while gogather0:
			ts_ret=ts.FindPic(0, 0, 1280, 340, "gatherpage.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gogather0=0
			else:
				prtitle(u'\t等待进入合卡球员页面')
				time.sleep(1)
		
		rr1=(0,0,163-10)
		trypair=1
		while trypair:
			rr1=ts.FindPic(653, rr1[2]+10, 1120, 584, bmpstr, "101010", 0.6, 0, pointer(intX), pointer(intX))
			#print u'找图',rr1	#14,9
			rr2=ts.FindPic(653, rr1[2]+5, 1120, 584, bmpstr, "101010", 0.6, 0, pointer(intX), pointer(intX))
			#print u'找图',rr2	#14,9
			if rr2[1]>0:
				#print '\tminus ',rr2[2]-rr1[2]
				if (rr2[2]-rr1[2]) in range(25,31):
					print u'\t找到一对'
					trypair=0
			else:
				print u'\t没有一对'
				return 2
				#trypair=0
		
		ts.MoveTo(rr1[1]+2, rr1[2]+3)	#选择球员
		ts.LeftClick()
		ts.MoveTo(rr2[1]+2, rr2[2]+3)	#选择球员
		ts.LeftClick()
		#ts.MoveTo(rr2[1]+2, rr2[2]+3+27)	#选择球员 备选2 1 1 1
		#ts.LeftClick()
		time.sleep(1)
		
		ts_ret=ts.FindPic(653, 163, 1120, 600, "tick.bmp", "151515", 0.4, 0, pointer(intX), pointer(intX))
		#print '\t',(ts_ret[2]-rr2[2])
		if (ts_ret[2]-rr2[2]) in range(-4,5):
			print u'\t修复2111'
			ts.MoveTo(rr2[1]+2, rr2[2]+3+27)	#选择球员 备选2 1 1 1
			ts.LeftClick()
			time.sleep(0.5)
		
		gogather1=1
		while gogather1:
			ts_ret=ts.FindPic(0, 0, 1280, 725, "gogather1.bmp", "151515", 0.4, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gogather1=0
			else:
				prtitle(u'\t等待进入合卡界面')
				time.sleep(1)
		ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#确定合卡
		ts.LeftClick()
		time.sleep(3)
		
		gogather2=1
		while gogather2:
			ts_ret=ts.FindPic(0, 0, 1280, 725, "gogather2.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gogather2=0
			else:
				prtitle(u'\t等待搞他')
				time.sleep(1)
		ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#搞他！！！！！！
		ts.LeftClick()
		time.sleep(4)
		
		gogather3=1
		while gogather3:
			ts_ret=ts.FindPic(370, 72, 1005, 236, "gathersucc.bmp", "101010", 0.3, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gogather3=0
				gatherresult[0]=gatherresult[0]+1
				print u'成了！',gatherresult
				r=1
			ts_ret=ts.FindPic(370, 72, 1005, 236, "gatherfail.bmp", "101010", 0.3, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gogather3=0
				gatherresult[1]=gatherresult[1]+1
				print u'跪了！',gatherresult
				r=0
			if gogather3:
				prtitle(u'\t等待合卡结果')
				time.sleep(1)
		
		gogather4=1
		while gogather4:
			ts_ret=ts.FindPic(0, 0, 1280, 725, "back.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))
			if ts_ret[1]>=0 and ts_ret[2]>=0:
				gogather4=0
			else:
				prtitle(u'\t等待返回')
				time.sleep(1)
		ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#返回
		ts.LeftClick()
		time.sleep(0.5)
		return r

	def gathercards():
		gatherresult=[[0,0],[0,0]]
		cardstogather=1
		ct=0
		while cardstogather:
			if EXIT:
				cardstogather=0

			if gatherplus('1.bmp',gatherresult[0])==2 and gatherplus('2.bmp',gatherresult[1])==2:
				print u'卡已合完'
				ct=ct+1
				if ct<3:
					for i in xrange(5):
						ts.MoveTo(1076, 533)	#向下拉
						ts.LeftClick()
				else:
					cardstogather=0
			else:
				ct=0
			
			if (gatherresult[0][0]+gatherresult[0][1]+gatherresult[1][0]+gatherresult[1][1])==0:
				print u'无卡！ 需解雇！进入休眠！'
			else:
				if (gatherresult[1][0]+gatherresult[1][1])==0:
					print u'合卡结果统计：\n+1: %s\t%f\n+2: %s\t' % (gatherresult[0],(gatherresult[0][0]/1.0/(gatherresult[0][0]+gatherresult[0][1])),gatherresult[1])
				elif (gatherresult[0][0]+gatherresult[0][1])==0:
					print u'合卡结果统计：\n+1: %s\t\n+2: %s\t%f' % (gatherresult[0],gatherresult[1],(gatherresult[1][0]/1.0/(gatherresult[1][0]+gatherresult[1][1])))
				else:
					print u'合卡结果统计：\n+1: %s\t%f\n+2: %s\t%f' % (gatherresult[0],(gatherresult[0][0]/1.0/(gatherresult[0][0]+gatherresult[0][1])),gatherresult[1],(gatherresult[1][0]/1.0/(gatherresult[1][0]+gatherresult[1][1])))	
	
	def buythis(bmpstr,got):
		ts_ret=ts.FindPic(536, 103, 943, 645, bmpstr, "101010", 0.6, 0, pointer(intX), pointer(intX))	#15,35
		if ts_ret[1]>=0 and ts_ret[2]>=0:
			print u'\t找到 +1'
			ts.MoveTo(ts_ret[1]+120, ts_ret[2]+8)	#选择球员
			time.sleep(0.1)
			ts.LeftClick()
			ts.LeftClick()
			
			time.sleep(0.1)
			ts.MoveTo(offx+1025, offy+638)	#购买
			ts.LeftClick()
			time.sleep(0.1)
			
			trytobuy=1
			while trytobuy:
				ts_ret=ts.FindPic(536, 103, 943, 645, "buy.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))	#15,35
				if ts_ret[1]>=0 and ts_ret[2]>=0:
					trytobuy=0
				else:
					print u'\t等待 buy'
					ts_ret=ts.FindPic(478, 590, 788, 685, "full.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))
					if ts_ret[1]>=0 and ts_ret[2]>=0:
						return 2	#阵容已满
					time.sleep(0.5)
			ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#确定购买
			ts.LeftClick()
			time.sleep(0.5)
			
			trybuy=1
			while trybuy:
				ts_ret=ts.FindPic(536, 103, 943, 645, "ok.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))	#15,35
				if ts_ret[1]>=0 and ts_ret[2]>=0:
					ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#已买到手
					ts.LeftClick()
					got[0]=got[0]+1
					print u'\t购买成功: ',got[0]
					trybuy=0
				else:
					ts_ret=ts.FindPic(536, 103, 943, 645, "okfail.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))	#15,35
					if ts_ret[1]>=0 and ts_ret[2]>=0:
						ts.MoveTo(ts_ret[1]+2, ts_ret[2]+2)	#没了
						ts.LeftClick()
						print u'\t购买失败'
						trybuy=0
					else:
						print u'\t等待 ok'
						time.sleep(1)
			return 1
		else:
			return 0
		
	def selectlast():
		ts.MoveTo(offx+713, offy+157)	#搜索候选
		ts.LeftClick()
		time.sleep(0.2)
		ts.MoveTo(offx+989, offy+229)	#进入球员
		time.sleep(0.1)
		ts.LeftClick()
		time.sleep(0.5)
		
	def selectfav(numfav):
		if numfav[0]>(playersinfav-1):
			numfav[0]=0
			numsel=0
			ts.MoveTo(offx+504, offy+225)	#向上拉到顶
			for i in xrange(20):
				ts.LeftClick()
			print u'一周走完休息40秒！'
			time.sleep(40)
		elif numfav[0]>13:	#一次有14个，从0计数
			ts.MoveTo(offx+504, offy+577)	#向下拉
			ts.LeftClick()
			numsel=13
			time.sleep(0.2)
		else:
			numsel=numfav[0]
		numfav[0]=numfav[0]+1
		ts.MoveTo(offx+476, offy+229+27*numsel)	#选择第几个
		time.sleep(0.1)
		ts.LeftClick()
		time.sleep(1)



	print u"TS插件已载入!\n1秒内将捕捉窗口，请将鼠标停留在模拟器/浏览器内!"
	time.sleep(1)
	hwnd = ts.GetMousePointWindow()

	print u"捕捉窗口： %d" % hwnd
	ts_ret = ts.BindWindow(hwnd, "normal", "windows", "windows", 1)	#gdi for genymotion
	time.sleep(1)

	if ts_ret==0:
		print u"窗口捕捉失败！"
		return
	
	#这里是脚本核心区域，请尽情修改。
	Mainloop=1
	while Mainloop:
		if usefav:
			selectfav(loopfav)	#选择默认下一个收藏球员
		else:
			selectlast()	#选择默认上一位球员
		
		tryplus1exist=1
		while tryplus1exist:
			launchbuy=buythis('1big.bmp',got)	#买+1卡
			if launchbuy==0:	#无卡
				tryplus1exist=0
			elif launchbuy==1:	#有卡已撸
				time.sleep(0.3)
			elif launchbuy==2:	#卡满 TODO
				print u'卡满应合卡！'
				sys.exit()
				gotogather()
				gathercards()	#未工作
				#gotoshop()
				#selectfav(buyfav)
	
		loop=loop+1
		print u'循环次数: %d\t已买人数: %d' % (loop,got[0])
		
		if EXIT:
			Mainloop=0
		time.sleep(tdelay)
		if EXIT:
			Mainloop=0

	"""
	ts_ret=ts.GetWindowRect(hwnd,pointer(intX),pointer(intX),pointer(intX),pointer(intX))
	print 'windowinfo',ts_ret
	if ts_ret[0]==1:
		windx=ts_ret[1]
		windy=ts_ret[2]
	else:
		windx=0
		windy=0
	"""
	
	"""一些可用的语句
	#ts_ret = ts.GetClientSize(hwnd, pointer(wide), pointer(high))
	#ts.FindPic(0, 0, 1080, 1080, "x.bmp", "000000", 0.1, 0, pointer(intX), pointer(intY))
	#ts.capture(0,0,100,100,"xxx.bmp")	
	#ts.KeyPressChar("Esc")
	
	set dm = createobject("dm.dmsoft")
	base_path = dm.GetBasePath()
	dm_ret = dm.SetPath(base_path)
	dm_ret = dm.SetDict(0,"dm_soft.txt")
	s = dm.Ocr(1050,793,1059,809,"aeafb1-303030",1.0)
	
	print 'find',ts.FindPic(536, 103, 943, 645, "1.bmp", "101010", 0.6, 0, pointer(intX), pointer(intX))	#15,35
	"""

class HK_exit(threading.Thread):
	def __init__(self):
		self.keyid=1
		self.flag=win32con.MOD_ALT
		self.key=win32con.VK_F10
		threading.Thread.__init__(self)
	def run(self):
		global EXIT
		user32 = ctypes.windll.user32
		if not user32.RegisterHotKey(None, self.keyid,self.flag,self.key):
			raise RuntimeError
		try:
			msg = ctypes.wintypes.MSG()
			while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
				if msg.message == win32con.WM_HOTKEY:
					if msg.wParam == self.keyid:
						EXIT = True
						print "Press run key to exit app."
						return
				user32.TranslateMessage(ctypes.byref(msg))
				user32.DispatchMessageA(ctypes.byref(msg))
		finally:
			user32.UnregisterHotKey(None, 1)
			
class HK_run(threading.Thread):
	def __init__(self):
		self.keyid=1
		self.flag=win32con.MOD_ALT
		self.key=win32con.VK_F10
		threading.Thread.__init__(self)
		
	def run(self):
		global EXIT
		user32 = ctypes.windll.user32
		if not user32.RegisterHotKey(None, self.keyid,self.flag,self.key):
			raise RuntimeError
		try:
			msg = ctypes.wintypes.MSG()
			while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
				if msg.message == win32con.WM_HOTKEY:
					if msg.wParam == self.keyid:
						if singlethreat:
							TS_run()
						else:
							t=threading.Thread(target=TS_run,args=())
							t.start()
				user32.TranslateMessage(ctypes.byref(msg))
				user32.DispatchMessageA(ctypes.byref(msg))
				if EXIT:
					return
		finally:
			user32.UnregisterHotKey(None, 1)


def main():
	print u'\n按下开始： Alt+F10.\n按下结束： Alt+F12!\n\n当前版本：%s' % ver
	if singlethreat:
		print u"进入单线程模式！"
	texit=HK_exit()
	texit.key=win32con.VK_F12
	texit.flag=win32con.MOD_ALT
	texit.start()
	
	trun=HK_run()
	trun.key=win32con.VK_F10
	trun.flag=win32con.MOD_ALT
	trun.start()
	
	texit.join()
	trun.join()

if __name__ == '__main__':
	main()