#coding: utf-8
"""
本脚本使用python27，支持windows。使用需安装python27以及插件pywin32，地址如下：
http://sourceforge.net/projects/pywin32/files/pywin32/
使用前还需下载天使插件TSPlug.dll，并注册Regsvr32 TSPlug.dll。
安装后直接双击脚本即可。按提示使用。
"""

import sys,os
reload(sys)
sys.setdefaultencoding('utf-8')
try:
	import fo3_getplayer as fo3get
except:
	sys.path.append('D:\\workplace\\program\\python\\python-TS-example\\fo3_getplayer')
	import fo3_getplayer as fo3get


fo3get.ver=u'Fifa 自动购买+1球员_当前球员版本 base r1'	#版本
fo3get.tdelay=5	#延迟时间，单位秒
fo3get.usefav=False
fo3get.singlethreat=True
os.system("title %s" % 'Lastplayer GET')

def main():
	print u'\n按下开始： Alt+F9.\n\n当前版本：%s' % fo3get.ver
	if fo3get.singlethreat:
		print u"进入单线程模式！"
	"""
	texit=HK_exit()
	texit.key=win32con.VK_F12
	texit.flag=win32con.MOD_ALT
	texit.start()
	"""
	
	trun=fo3get.HK_run()
	trun.key=fo3get.win32con.VK_F9
	trun.flag=fo3get.win32con.MOD_ALT
	trun.start()
	
	#texit.join()
	trun.join()

if __name__ == '__main__':
	main()