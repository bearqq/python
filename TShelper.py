#coding: utf-8
"""
本脚本使用python27，支持windows。使用需安装python27以及插件pywin32，地址如下：
http://sourceforge.net/projects/pywin32/files/pywin32/
使用前还需下载天使插件TSPlug.dll，并注册Regsvr32 TSPlug.dll。
安装后直接双击脚本即可。按提示使用。
"""

#defines
ver=u'Base r3'	#版本

#imports
import ctypes, win32con, ctypes.wintypes, sys
import win32com.client
import time
from ctypes import *
import threading
import logging

reload(sys)
sys.setdefaultencoding('utf-8')

#inits
logging.basicConfig(
        format='[%(asctime)s][%(name)s][%(levelname)s] - %(message)s',
        datefmt='%Y-%d-%m %H:%M:%S',
        level=logging.DEBUG,
    )
log = logging.getLogger('HOTKEY.lib')

try:
	#dm=win32com.client.Dispatch('dm.dmsoft')
	ts=win32com.client.Dispatch('ts.tssoft')
except:
	log.critical(u"请先注册插件 Regsvr32 TSPlug.dll")
	sys.exit()

	#ts.SetPath("D:\workplace\program\python\ptrhon-TS-fifa\plugin")

def make_flag():return [0]
EXIT = make_flag()

key_dict={'backspace':0x08,
	'tab':0x09,
	'clear':0x0C,
	'enter':0x0D,
	'shift':0x10,
	'ctrl':0x11,
	'alt':0x12,
	'pause':0x13,
	'caps_lock':0x14,
	'esc':0x1B,
	'spacebar':0x20,
	'page_up':0x21,
	'page_down':0x22,
	'end':0x23,
	'home':0x24,
	'left_arrow':0x25,
	'up_arrow':0x26,
	'right_arrow':0x27,
	'down_arrow':0x28,
	'select':0x29,
	'print':0x2A,
	'execute':0x2B,
	'print_screen':0x2C,
	'ins':0x2D,
	'del':0x2E,
	'help':0x2F,
	'0':0x30,
	'1':0x31,
	'2':0x32,
	'3':0x33,
	'4':0x34,
	'5':0x35,
	'6':0x36,
	'7':0x37,
	'8':0x38,
	'9':0x39,
	'a':0x41,
	'b':0x42,
	'c':0x43,
	'd':0x44,
	'e':0x45,
	'f':0x46,
	'g':0x47,
	'h':0x48,
	'i':0x49,
	'j':0x4A,
	'k':0x4B,
	'l':0x4C,
	'm':0x4D,
	'n':0x4E,
	'o':0x4F,
	'p':0x50,
	'q':0x51,
	'r':0x52,
	's':0x53,
	't':0x54,
	'u':0x55,
	'v':0x56,
	'w':0x57,
	'x':0x58,
	'y':0x59,
	'z':0x5A,
	'numpversion.infoad_0':0x60,
	'numpad_1':0x61,
	'numpad_2':0x62,
	'numpad_3':0x63,
	'numpad_4':0x64,
	'numpad_5':0x65,
	'numpad_6':0x66,
	'numpad_7':0x67,
	'numpad_8':0x68,
	'numpad_9':0x69,
	'multiply_key':0x6A,
	'add_key':0x6B,
	'separator_key':0x6C,
	'subtract_key':0x6D,
	'decimal_key':0x6E,
	'pide_key':0x6F,
	'F1':0x70,
	'F2':0x71,
	'F3':0x72,
	'F4':0x73,
	'F5':0x74,
	'F6':0x75,
	'F7':0x76,
	'F8':0x77,
	'F9':0x78,
	'F10':0x79,
	'F11':0x7A,
	'F12':0x7B,
	'F13':0x7C,
	'F14':0x7D,
	'F15':0x7E,
	'F16':0x7F,
	'F17':0x80,
	'F18':0x81,
	'F19':0x82,
	'F20':0x83,
	'F21':0x84,
	'F22':0x85,
	'F23':0x86,
	'F24':0x87,
	'num_lock':0x90,
	'scroll_lock':0x91,
	'left_shift':0xA0,
	'right_shift ':0xA1,
	'left_control':0xA2,
	'right_control':0xA3,
	'left_menu':0xA4,
	'right_menu':0xA5,
	'browser_back':0xA6,
	'browser_forward':0xA7,
	'browser_refresh':0xA8,
	'browser_stop':0xA9,
	'browser_search':0xAA,
	'browser_favorites':0xAB,
	'browser_start_and_home':0xAC,
	'volume_mute':0xAD,
	'volume_Down':0xAE,
	'volume_up':0xAF,
	'next_track':0xB0,
	'previous_track':0xB1,
	'stop_media':0xB2,
	'play/pause_media':0xB3,
	'start_mail':0xB4,
	'select_media':0xB5,
	'start_application_1':0xB6,
	'start_application_2':0xB7,
	'attn_key':0xF6,
	'crsel_key':0xF7,
	'exsel_key':0xF8,
	'play_key':0xFA,
	'zoom_key':0xFB,
	'clear_key':0xFE,
	'+':0xBB,
	',':0xBC,
	'-':0xBD,
	'.':0xBE,
	'/':0xBF,
	'`':0xC0,
	';':0xBA,
	'[':0xDB,
	'//':0xDC,
	']':0xDD,
	"'":0xDE,
	'`':0xC0}

class HK_exit(threading.Thread):
	def __init__(self,EXIT=EXIT):
		self.keyid=1
		self.flag=win32con.MOD_ALT
		self.key=win32con.VK_F10
		self.EXIT=EXIT
		threading.Thread.__init__(self)

	def run(self):
		user32 = ctypes.windll.user32
		if not user32.RegisterHotKey(None, self.keyid,self.flag,self.key):
			raise RuntimeError
		try:
			msg = ctypes.wintypes.MSG()
			while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
				if msg.message == win32con.WM_HOTKEY:
					if msg.wParam == self.keyid:
						self.EXIT[0] = 1
						log.critical("Press run key to exit app.")
						return
				user32.TranslateMessage(ctypes.byref(msg))
				user32.DispatchMessageA(ctypes.byref(msg))
		finally:
			user32.UnregisterHotKey(None, 1)

def HK_exit_daemon(EXIT=EXIT,key=win32con.VK_F12,flag=win32con.MOD_ALT):
	texit=HK_exit(EXIT)
	texit.key=key
	texit.flag=flag
	texit.start()
	return texit

class HK_run(threading.Thread):
	"""flag key singlethreat EXIT offx offy triggered()"""
	#http://www.mamicode.com/info-detail-1319197.html
	def __init__(self,EXIT=EXIT):
		self.keyid=1
		self.flag=win32con.MOD_ALT
		self.key=win32con.VK_F10
		self.singlethreat=False	#设置为False可以多次多地触发，按下停止后全部停止，但主程序需要再次按下触发按键才能退出。设置为True表示只能一个窗口触发主程序，可即时退出。
		#self.tdelay=0.2	#延迟时间，单位秒
		#self.offx=0	#x轴修正值，修改后按键位置会整体偏移
		#self.offy=0	#y轴修正值，修改后按键位置会整体偏移
		self.EXIT=EXIT
		threading.Thread.__init__(self)
	
	def triggered(self):
		pass

	def run(self):
		user32 = ctypes.windll.user32
		if not user32.RegisterHotKey(None, self.keyid,self.flag,self.key):
			raise RuntimeError
		try:
			msg = ctypes.wintypes.MSG()
			while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
				if msg.message == win32con.WM_HOTKEY:
					if msg.wParam == self.keyid:
						if self.singlethreat:
							self.triggered()
						else:
							t=threading.Thread(target=self.triggered,args=())
							t.start()
				user32.TranslateMessage(ctypes.byref(msg))
				user32.DispatchMessageA(ctypes.byref(msg))
				if self.EXIT[0]:
					return
		finally:
			user32.UnregisterHotKey(None, 1)

def main():
	log.critical(u'\n按下开始： Alt+Z\n按下结束： Alt+F12!\n当前版本：%s' % ver)
	
	def trig():
		log.critical('Triggered')
	
	THE_EXIT=make_flag()

	trun=HK_run(THE_EXIT)#HK_run(THE_EXIT,90,win32con.MOD_ALT)
	trun.key=90#z
	trun.flag=win32con.MOD_ALT
	trun.triggered=trig
	trun.start()
	
	HK_exit_daemon(THE_EXIT)
	trun.join()

if __name__ == '__main__':
	main()


"""
def trig():
	print "happy!"
	time.sleep(3)


	#wide=c_int(0)
	#high=c_int(0)
	#intX=c_int(0)
	#intY=c_int(0)

	#print u"TS插件已载入!\n3秒内将捕捉窗口，请将鼠标停留在模拟器/浏览器内!"
	#time.sleep(3)
	#hwnd = ts.GetMousePointWindow()	
	#print u"捕捉窗口： %d" % hwnd
	#ts_ret = ts.BindWindow(hwnd, "gdi", "windows", "windows", 1)	#gdi for genymotion
	#time.sleep(2)

	#if ts_ret==0:
		#print u"窗口捕捉失败！"
		#return
	
	#一些可用的语句
	#ts_ret = ts.GetClientSize(hwnd, pointer(wide), pointer(high))
	#ts.FindPic(0, 0, 1080, 1080, "x.bmp", "000000", 0.1, 0, pointer(intX), pointer(intY))
	#ts.capture(0,0,100,100,"xxx.bmp")	
	#ts.KeyPressChar("Esc")
"""