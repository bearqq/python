#coding: utf-8
"""
本脚本使用python27，支持windows。使用需安装python27以及插件pywin32，地址如下：
http://sourceforge.net/projects/pywin32/files/pywin32/
使用前还需下载天使插件TSPlug.dll，并注册Regsvr32 TSPlug.dll。
安装后直接双击脚本即可。按提示使用。
"""

#defines
ver=u'Fifa Loop购买， base r2'	#版本
singlethreat=False	#设置为False可以多次多地触发，按下停止后全部停止，但主程序需要再次按下触发按键才能退出。设置为True表示只能一个窗口触发主程序，可即时退出。
tdelay=0.2	#延迟时间，单位秒
offx=0	#x轴修正值，修改后按键位置会整体偏移
offy=0	#y轴修正值，修改后按键位置会整体偏移
indebug=False

#imports
import ctypes, win32con, ctypes.wintypes, win32gui,sys
import win32com.client
import time
from ctypes import *
import threading
reload(sys)
sys.setdefaultencoding('utf-8')

#inits
EXIT = False
loop=0

try:
	#dm=win32com.client.Dispatch('dm.dmsoft')
	ts=win32com.client.Dispatch('ts.tssoft')
except:
	print u"请先注册插件 Regsvr32 TSPlug.dll"
	sys.exit()

if indebug:
	ts.SetPath("D:\workplace\program\python\ptrhon-TS-fifa\plugin")
else:
	ts.SetPath("plugin")

"""
def TS_run():
	print "happy!"
	time.sleep(3)
"""
i=0
def TS_run(): #main function
	global loop,i
	#wide=c_int(0)
	#high=c_int(0)
	#intX=c_int(0)
	#intY=c_int(0)

	#print u"TS插件已载入!\n3秒内将捕捉窗口，请将鼠标停留在模拟器/浏览器内!"
	#time.sleep(3)
	#hwnd = ts.GetMousePointWindow()	
	#print u"捕捉窗口： %d" % hwnd
	#ts_ret = ts.BindWindow(hwnd, "gdi", "windows", "windows", 1)	#gdi for genymotion
	#time.sleep(2)

	#if ts_ret==0:
		#print u"窗口捕捉失败！"
		#return
	
	"""一些可用的语句
	#ts_ret = ts.GetClientSize(hwnd, pointer(wide), pointer(high))
	#ts.FindPic(0, 0, 1080, 1080, "x.bmp", "000000", 0.1, 0, pointer(intX), pointer(intY))
	#ts.capture(0,0,100,100,"xxx.bmp")	
	#ts.KeyPressChar("Esc")
	"""
	
	#这里是脚本核心区域，请尽情修改。
	#for i in (0,):
	ts.MoveTo(offx+978, int(offy+259+27.5*i))
	time.sleep(0.2)
	ts.LeftClick()
	time.sleep(0.5)
	ts.KeyPressChar("Back")
	i=i+1
	if i>13:
		i=1
		ts.MoveTo(offx+1088, offy+624)
		for q in xrange(13):
			ts.LeftClick()
	loop=loop+1
	print u'循环次数: %d' % loop

def TS_run2(): #main function
	global loop
	#这里是脚本核心区域，请尽情修改。
	#for i in (0,):
	ts.LeftClick()
	time.sleep(0.1)
	ts.MoveTo(offx+672, offy+259)
	ts.LeftClick()
	time.sleep(tdelay)
	ts.MoveTo(offx+1017, offy+674)
	ts.LeftClick()
	time.sleep(tdelay+0.2)
	ts.KeyPressChar("Enter")
	ts.KeyPressChar("Enter")
	ts.KeyPressChar("Enter")
	#ts.MoveTo(offx+466, offy+392)
	ts.KeyPressChar("Enter")
	time.sleep(tdelay+0.5)
	ts.KeyPressChar("Enter")		

	loop=loop+1
	print u'循环次数: %d' % loop

class HK_exit(threading.Thread):
	def __init__(self):
		self.keyid=1
		self.flag=win32con.MOD_ALT
		self.key=win32con.VK_F10
		threading.Thread.__init__(self)
	def run(self):
		global EXIT
		user32 = ctypes.windll.user32
		if not user32.RegisterHotKey(None, self.keyid,self.flag,self.key):
			raise RuntimeError
		try:
			msg = ctypes.wintypes.MSG()
			while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
				if msg.message == win32con.WM_HOTKEY:
					if msg.wParam == self.keyid:
						EXIT = True
						print "Press run key to exit app."
						return
				user32.TranslateMessage(ctypes.byref(msg))
				user32.DispatchMessageA(ctypes.byref(msg))
		finally:
			user32.UnregisterHotKey(None, 1)
			
class HK_run2(threading.Thread):
	def __init__(self):
		self.keyid=1
		self.flag=win32con.MOD_ALT
		self.key=win32con.VK_F10
		threading.Thread.__init__(self)
		
	def run(self):
		global EXIT
		user32 = ctypes.windll.user32
		if not user32.RegisterHotKey(None, self.keyid,self.flag,self.key):
			raise RuntimeError
		try:
			msg = ctypes.wintypes.MSG()
			while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
				if msg.message == win32con.WM_HOTKEY:
					if msg.wParam == self.keyid:
						if singlethreat:
							TS_run2()
						else:
							t=threading.Thread(target=TS_run2,args=())
							t.start()
				user32.TranslateMessage(ctypes.byref(msg))
				user32.DispatchMessageA(ctypes.byref(msg))
				if EXIT:
					return
		finally:
			user32.UnregisterHotKey(None, 1)

class HK_run(threading.Thread):
	def __init__(self):
		self.keyid=1
		self.flag=win32con.MOD_ALT
		self.key=win32con.VK_F10
		threading.Thread.__init__(self)
		
	def run(self):
		global EXIT
		user32 = ctypes.windll.user32
		if not user32.RegisterHotKey(None, self.keyid,self.flag,self.key):
			raise RuntimeError
		try:
			msg = ctypes.wintypes.MSG()
			while user32.GetMessageA(ctypes.byref(msg), None, 0, 0) != 0:
				if msg.message == win32con.WM_HOTKEY:
					if msg.wParam == self.keyid:
						if singlethreat:
							TS_run()
						else:
							t=threading.Thread(target=TS_run,args=())
							t.start()
				user32.TranslateMessage(ctypes.byref(msg))
				user32.DispatchMessageA(ctypes.byref(msg))
				if EXIT:
					return
		finally:
			user32.UnregisterHotKey(None, 1)

def main():
	print u'\n按下开始： Alt+Z.\n按下购买： Alt+X.\n按下结束： Alt+F12!\n\n当前版本：%s' % ver
	if singlethreat:
		print u"进入单线程模式！"
	texit=HK_exit()
	texit.key=win32con.VK_F12
	texit.flag=win32con.MOD_ALT
	texit.start()
	
	trun=HK_run()
	trun.key=90
	trun.flag=win32con.MOD_ALT
	trun.start()
	
	trun2=HK_run2()
	trun2.key=88
	trun2.flag=win32con.MOD_ALT
	trun2.start()
	
	texit.join()
	trun.join()
	trun2.join()

if __name__ == '__main__':
	main()